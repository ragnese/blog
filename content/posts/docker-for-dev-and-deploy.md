+++
title = "How I Use Docker for Development and Deployment (For Now)"
author = ["Rob Agnese"]
date = 2019-01-25T22:35:00-05:00
lastmod = 2019-01-26T08:52:38-05:00
tags = ["docker", "php"]
draft = false
+++

## Prelude {#prelude}

I've recently begun switching some software projects from Vagrant to Docker
(yes, I'm aware that you can use [both](https://www.vagrantup.com/docs/provisioning/docker.html)). But I found the process of setting up
a non-trivial project to be very frustrating for a few reasons.

Docker proponents
say that one of the main value propositions of Docker is that you can have the
same development environment as your production environment. However, it's not
actually that simple to have a working development setup that matches
your final, deployed, app. If your development process involves
things like running unit tests, or compiling with debug options, etc, your development
environment probably has requirements that your production does not.
It was not obvious to me how I should proceed so that I could develop and deploy
against very similar environments. I'm not
the only one who had trouble as can be seen from a quick web search. But most
of the solutions either weren't what I wanted from my development workflow
(use separate Dockerfiles), or super vague ("Use multi-stage builds.").

This post assumes you have read at least one or two Docker tutorials and that you basically understand
the difference between an "image" and a "container", know what Docker Compose is, and that you are vaguely
familiar with the commands in a Dockerfile as well as those in a docker-compose.yml.


## A Typical Docker Setup For Deployment {#a-typical-docker-setup-for-deployment}

I'll start right where most introductory Docker tutorials end: with an almost trivial Docker set up for a perfect,
100% complete, application that you will never need to fix or add to. Since I just recently dockerized a PHP
application, I'll use PHP for my examples. Here's what our directory might look like:

```shell
.
├── src
│   └── App.php
├── tests
│   └── AppTest.php
├── .dockerignore
├── composer.json
├── composer.lock
├── Dockerfile
└── docker-compose.yml
```

I'll (very) briefly go over each of the docker-related files.

```shell
# .dockerignore
.git
.gitignore
.dockerignore
Dockerfile
docker-compose.yml
tests
```

The .dockerignore file is much like a .gitignore file. It keeps the listed files/patterns from being copied
into your Docker image when your Dockerfile issues an ADD or COPY command.

```shell
# Dockerfile
FROM composer:1.8 AS composer
COPY . .
RUN composer install -n --no-dev

FROM php7-cli-alpine
WORKDIR my-app
RUN apk add --no-cache some-package && do-some-prep-work && \
        mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY --from=composer /app . # because composer:1.8 image sets WORKDIR to /app
```

First thing to note is that the official PHP Docker images do not contain Composer, the de facto PHP
dependency manager. I'm using a multistage build approach in this file because we don't need Composer
in our final image. So we basically just use the Composer image to install our dependencies for us and
then throw it away. This is a comon pattern in Dockerfiles. As you can see, we also need to install some
packages and/or do some configuration for our app to work correctly.

```shell
# docker-compose.yml
version '3'

services:
  my-app:
    build: .
```

You really don't need a Docker compose file if your project is actually this simple. But I have one there
because I know that we will need it later.

This is basically where most Docker tutorials leave you. "Please share and like and tweet and give me the clap".


## Docker Setup For Development And Deployment {#docker-setup-for-development-and-deployment}


### The Goal {#the-goal}

But what if my development machine has a different version of Composer installed? Or PHP? What if I'm not even running
Linux at all (why aren't you?)? I have to just code in the dark, build my final app and only **then** get to see if it works?
Maybe that's how some people are doing it- I don't know. But that wont work for me, so let's figure out what we want from
our fancy Docker set up. I've decided on these ideal requirements:

1.  <a id="org7d4bb39"></a> I don't want to use my host PHP or Composer for anything. Ideally, I don't even want PHP and Composer installed on my host.
2.  <a id="orgd2cedc8"></a> I should be able to edit and run tests "seamlessly" for some definition of "seamless". Said tests should run in the same environment as the production app.
3.  <a id="org9491765"></a> I do **not** want to have separate Dockerfiles for dev and prod. That defeats the goal of knowing my dev environment matches my production environment (what if I add a dependency during development and forget to update the production Dockerfile to match it?).
4.  <a id="org3dea5c3"></a> I want my production image to be exactly as small as the example above. No dev dependencies should be baked in to a prod image.

That's a big ask, I know. But I believe I have gotten most of the way there.


### The Result {#the-result}

I spent several hours trying different approaches, so I wont take you through a guided tour of my failed attempts. Instead, I'll show
you the fruits of my labor and explain how I've accomplished my four goals, and what I feel is still lacking. The tl;dr of it is
that I use a combination of [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/), [volumes](https://docs.docker.com/storage/volumes/), and [docker-compose overrides](https://docs.docker.com/compose/reference/overview/#specifying-multiple-compose-files) to allow me to build a dev image and a prod image
from the same common base image, and then share my local directory with the dev image for development.

Here is my final directory structure:

```nil
.
├── src
│   └── App.php
├── tests
│   └── AppTest.php
├── .dockerignore
├── composer.json
├── composer.lock
├── Dockerfile
├── docker-compose.yml
├── docker-compose.override.yml
└── docker.sh
```

You'll notice that I've added two files. If you run `docker-compose` without specifying a file for it, will automatically
look for docker-compose.yml and docker-compose.override.yml and apply the overrides from the latter file to the former. So our
production stuff now lives in docker-compose.yml, while docker-compose.override.yml contains stuff to make development nicer.

Let's go back through the files to see what's changed:

```shell
# .dockerignore
.git
.gitignore
.dockerignore
Dockerfile
docker-compose.yml
docker-compose.override.yml
docker.sh
vendor/
tests/
```

We added our new files to the .dockerignore file. Nothing interesting there.

```shell
# Dockerfile
FROM php7-cli-alpine AS base
WORKDIR my-app
RUN apk add --no-cache some-package && do-some-prep-work && \
        apk --no-cache add git unzip # for composer
COPY --from=composer:1.8 /usr/bin/composer /opt/bin/

FROM base AS dev
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

FROM base AS prod
COPY . .
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
        COMPOSER_ALLOW_SUPERUSER=1 php /opt/bin/composer install --no-dev -n && \
        apk del git unzip && \
        rm /opt/bin/composer
```

This is where it gets interesting. First of all, I removed the composer stage from our build. Instead, we'll
have to run composer differently for our prod and dev builds. Notice that we _do_ still reference the official `composer:1.8` image-
this time, though, we just steal the executable from it, which we now have to remove explicitly in the prod image.

You can also see that I have a common "base" image that our dev and prod environments both depend on.
Using docker's [--target flag](https://docs.docker.com/develop/develop-images/multistage-build/#stop-at-a-specific-build-stage), we can instruct our build to stop at "dev" or "prod". The bad part about
this setup is that when we do a production build, we do some unnecessary work: we do a RUN for the dev image even when
don't use any of the results. Luckily, it's a tiny amount of work, and will be cached anyway.

You may be wondering why we don't do something like this:

```shell
# Dockerfile
FROM php7-cli-alpine AS base
WORKDIR my-app
RUN apk add --no-cache some-package && do-some-prep-work && \
        apk add --no-cache git unzip # for composer
COPY --from=composer:1.8 /usr/bin/composer /opt/bin/
COPY . .

FROM base
ARG MODE
# From memory, so I probably got my semicolons incorrect
RUN if [ "$MODE" = "prod" ]; then && \
        mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
        COMPOSER_ALLOW_SUPERUSER=1 php /opt/bin/composer install --no-dev -n && \
        apk del git unzip && \
        rm /opt/bin/composer; \
        else \
        mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"; \
fi
```

The problem with this is that if you try to switch between a prod build and a dev build without
any other changes, the final RUN command will **not** be rerun. Docker will see that no files have
changed, and that your Dockerfile images have the same commands as cached images, so it will just reuse
a cached image. Changing the value of ARG does not seem to invalidate the cache.

You may also be wondering why we don't copy our app or run Composer in the dev layer. We're going to mount
our host directory as a shared volume with the dev container, so the files will be shared between the host
and the container. Thus, we don't want or need any source code to be baked into the image.

```shell
# docker-compose.yml
version '3.4'

services:
  app:
    build:
      context: .
      target: prod
```

Notice that we had to bump the file version number for the `target` property. This file should be self-explanatory in
light of the changes to our Dockerfile.

At this point, we have accomplished/maintained our goals [#3](#org9491765), and [#4](#org3dea5c3). When we build with either `docker build --target=prod .` or
`docker-compose -f docker-compose.yml build`, we'll get the same, minimal production image we started with. When we build
with `docker build --target=dev` or `docker-compose build`, we will get our dev image.

```shell
# docker-compose.override.yml
version '3.4'

services:
  app:
    build:
      target: dev
    entrypoint: php
    volumes:
      - .:/my-app
```

Here is where we accomplish goals [#1](#org7d4bb39) and [#2](#orgd2cedc8), mostly. Remember that we don't actually `COPY` our files into our dev image, nor
do we run Composer to fetch our dependencies. Instead, we'll use a Docker volume to share our host app directory with our dev
containers. Since volumes are, by default, read-write, any changes we make on our host will be immediately available when we
run a container against our dev image. Likewise, if we, for example, run `composer install` while inside of a dev container, the `vendor/`
directory in our host will be populated (along with any other build artifacts and file changes from Composer scripts, etc).

Now, by itself, this feels a bit awkward to use (not what I'd call seamless), so I also slapped together `docker.sh` to simplify
interacting with our project images.

```shell
#!/bin/bash

set -euo pipefail

function print_usage {
    cat <<EOF
Usage: $0 [command] <command_args>

build                               Build a development docker image and cleanup old versions
clean                               Stop and remove all project-related docker images
composer <command> <args...>        Run composer inside of the development image
shell                               Open a shell on the development image
test <args...>                      Run unit tests on the development image
deploy                              Build the production image
-h, --help                          Display this message
EOF
}

if [[ "$#" -eq 0 ]]; then
    print_usage
    exit
fi

case "$1" in
    "build")
        docker-compose build
        docker-compose run --rm app /opt/bin/composer install --no-interaction
        ;;
    "clean")
        docker-compose down -v --rmi all --remove-orphans > /dev/null 2>&1
        docker system prune -f --volumes
        ;;
    "composer")
        docker-compose run --rm app /opt/bin/composer "${@:2}"
        ;;
    "shell")
        docker-compose run --rm --entrypoint sh app
        ;;
    "test")
        docker-compose run --rm app ./vendor/bin/phpunit tests "${@:2}"
        ;;
    "deploy")
        docker-compose -f docker-compose.yml down -v --rmi all --remove-orphans > /dev/null 2>&1
        docker-compose -f docker-compose.yml build --force-rm --no-cache
        docker system prune -f --volumes
        ;;
    "-h"|"--help"|*)
        print_usage
esac
```

This makes interacting with our project a lot less painful. Basically, when you first
check out your project, you'll run `./docker.sh build`. After that you'll only need to
run that command if you change your OS-level dependencies. You install Composer dependencies
via `./docker.sh composer install foobar` and run your tests with `./docker.sh test`.
When your app is awesome, you do `./docker.sh deploy` and then your app is ready to be run
with `docker run my-app_app:latest`.

The only goal I've fallen short on is [#1](#org7d4bb39). I like to use Emacs for development with [lsp-mode](https://github.com/emacs-lsp/lsp-mode) (even
though I'll admit that the PHP language server is not that great). So to get completion
recommendations, it means that I do need PHP installed on my host machine to run the PHP language
server, which is written in PHP. So maybe that doesn't really count? I don't know.

In any case, if you'd like to use a similar structure for your projects, I have an example repo
[here](https://gitlab.com/ragnese/php-docker-skeleton).

Keep in mind that I was fairly loosey-goosey here with pinning down version numbers. You'll, of
course, want to be very explicit in your dependencies, include the exact version of PHP, Alpine (or
whatever OS base you use), the OS-level deps that are installed in the base image, etc.

Happy bug-fixing!
